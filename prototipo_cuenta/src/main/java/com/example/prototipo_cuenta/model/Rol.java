package com.example.prototipo_cuenta.model;

import javax.persistence.*;

@Entity
public class Rol {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_rol", nullable = false)
    private String idRol;

    @Column(name = "nombre_roles", nullable = false)
    private String nombreRoles;

    public Rol() {

    }

    public Rol(String idRol, String nombreRoles) {
        this.idRol = idRol;
        this.nombreRoles = nombreRoles;
    }

    public String getIdRol() {
        return idRol;
    }

    public void setIdRol(String idRol) {
        this.idRol = idRol;
    }

    public String getNombreRoles() {
        return nombreRoles;
    }

    public void setNombreRoles(String nombreRoles) {
        this.nombreRoles = nombreRoles;
    }
}
