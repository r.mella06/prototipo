package com.example.prototipo_cuenta.repository;

import com.example.prototipo_cuenta.model.Usuario;

import org.springframework.data.repository.CrudRepository;

public interface  UsuarioRepository extends CrudRepository<Usuario, Long>{

}
