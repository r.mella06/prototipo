package com.example.prototipo_cuenta.repository;

import com.example.prototipo_cuenta.model.Rol;


import org.springframework.data.repository.CrudRepository;

public interface RolRepository extends CrudRepository<Rol, Long>{
}
