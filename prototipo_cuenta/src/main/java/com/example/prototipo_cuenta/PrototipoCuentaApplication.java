package com.example.prototipo_cuenta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrototipoCuentaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrototipoCuentaApplication.class, args);
	}

}
